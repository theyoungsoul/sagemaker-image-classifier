Inspired by:
"AWS Innovate | Intro to Deep Learning: Building an Image Classifier on Amazon SageMaker"
https://youtu.be/KCzgR7eQ3PY

https://github.com/youngsoul/jupyter-notebooks

install dependencies with setup.sh
ln opencv, cv2 into project.
ln -s /usr/local/Cellar/opencv/4.0.1/lib/python3.7/site-packages/cv2/python-3.7/cv2.cpython-37m-darwin.so venv/lib/python3.6/site-packages/cv2.so

find im2rec.py from mxnet, so we can convert the image files to recordio formats which is what sagemaker wants.

put this file into the base of the application for each access.

find . -name 'im2rec.py'
./venv/lib/python3.6/site-packages/tools/im2rec.py

cp ./venv/lib/python3.6/site-packages/tools/im2rec.py <project root>


./im2rec_script.sh

make a note of the output as those are the training label values:
cats 0
dogs 1
panda 2

