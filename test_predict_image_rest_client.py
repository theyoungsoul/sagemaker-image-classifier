import requests
import base64

api_gateway_url = "https://vojpv3knd4.execute-api.us-east-1.amazonaws.com/test/predictimageclass"

labels = ['Cat', 'Dog', 'Ernest T Bass', 'Panda', 'Water Bottle']

if __name__ == '__main__':
    def classify_deployed(file_name):
        payload = None
        with open(file_name, 'rb') as f:
            payload = f.read()
            payload = bytearray(payload)

        return payload


    test_images = [
        (labels[0],
         '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/cats/cats_00009.jpg'),
        (labels[1],
         '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/dogs/dogs_00024.jpg'),
        (labels[2],
         '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/ernest_t_bass/ernest_t_bass_SH7lK.png'),
        (labels[3],
         '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/panda/panda_00024.jpg'),
        (labels[4],
         '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/water_bottle/water_bottle_1Cgdy.png')
    ]

    for test_label, test_image in test_images:

        payload_body = classify_deployed(test_image)


        payload_body = base64.b64encode(payload_body).decode()
        payload = {
            'data': payload_body
        }
        response = requests.post(api_gateway_url, data=None, json=payload)

        response_json = response.json()
        print(f"Status Coce: {response_json['statusCode']}")
        print(f"Predicted Label: {response_json['body']['predicted_label']}")
        print(f"Predicted Probability: {response_json['body']['predicted_probability']}")
        print(f"Predictions: {response_json['body']['predictions']}")
        print("-------------------")
