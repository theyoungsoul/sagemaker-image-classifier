import boto3
import sys
import json
import operator

session = boto3.Session(profile_name='spr')
client = session.client("sagemaker-runtime", region_name='us-east-1')


def classify_deployed(file_name):
    payload = None
    with open(file_name, 'rb') as f:
        payload = f.read()
        payload = bytearray(payload)

    return payload

labels = ['Cat', 'Dog', 'Ernest T Bass', 'Panda', 'Water Bottle']

test_images = [
    (labels[0], '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/cats/cats_00009.jpg'),
    (labels[1], '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/dogs/dogs_00024.jpg'),
    (labels[2], '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/ernest_t_bass/ernest_t_bass_SH7lK.png'),
    (labels[3], '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/panda/panda_00024.jpg'),
    (labels[4], '/Users/patryan/Development/mybitbucket/sagemaker-image-classifier/data/data_resize_224_224/water_bottle/water_bottle_1Cgdy.png')
]

if __name__ == '__main__':

    for test_label, test_image in test_images:
        payload_body = classify_deployed(test_image)

        response = client.invoke_endpoint(
            EndpointName="image-class-endpoint",
            Body=payload_body,
            ContentType='application/x-image'
            )

        predictions = json.loads(response['Body'].read())

        print(predictions)
        max_index, max_value = max(enumerate(predictions), key=operator.itemgetter(1))

        print(labels[max_index])
        assert test_label == labels[max_index]

