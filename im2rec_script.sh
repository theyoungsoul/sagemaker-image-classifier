#!/usr/bin/env bash

GENERATED_FILES_PREFIX="animals"
DATA_DIR ="./data/animals"
CLASSES_FILENAME ="animals_classes.txt"

echo "Creating LST file"
python im2rec.py --list --recursive --pass-through --test-ratio=0.3 --train-ratio=0.7 animals_prefix ./data/animals > animals_classes.txt
cat animals_classes.txt

echo "Creating RecordIO files in data/animals_recordio"
python im2rec.py --num-thread=4 animals_prefix_train.lst ./data/animals
python im2rec.py --num-thread=4 animals_prefix_test.lst ./data/animals

